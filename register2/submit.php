<?php

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error="";
$error_text="";
$facultate="";
$sex="";

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}
if(isset($_POST['facultate'])){
$facultate=$_POST['facultate'];
}

if((!is_numeric($phone)) || (strlen($phone)!=10))
{
	echo $phone;
	$error=1;
	$error_text="Nr tel gresit";
}


if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($department) || empty($birth) || empty($question) || empty($captcha_inserted) || empty($check) || empty($facultate))
{
	$error=1;
	$error_text="Empty case";
}

if(strlen( $firstname) <3 || strlen($lastname) <3 )
{
	$error=1;
	$error_text="first/last name too short";
}

if( strlen($firstname)>20 || strlen($lastname)>20)
{
	$error=1;
	$error_text="first/last name too long";
}

if( strlen($question)<15)
{
	$error=1;
	$error_text="question too short";
}

if( !ctype_alpha($firstname) || !ctype_alpha($lastname))
{
	$error=1;
	$error_text="first/last name has a number";
}

$c=(int)($cnp/pow(10,12));

if(strlen($c)!=1 || $c>6 )
{
	$error=1;
	$error_text="CNP inccorect";
}
else if($c%2 == 0) $sex="F";
else  $sex="M";

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  $error = 1;
	$error_text="email invalid";
}

if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$facebook))
 {
  $error_text = "Invalid URL facebook";
	$error=1;
}

if($captcha_inserted!=$captcha_generated)
{
	$error=1;
	$error_text="cod captcha invalid";
}

if(strlen($facultate)<3 || strlen($facultate)>30)
{
	$error=1;
	$error_text="incorrect facultate";
}

$date = date('Y/m/d ', time());
$long = strtotime($birth);
$lo = strtotime($date);


if( $lo-$long<18*365*24*60*60)
{
	$error=1;
	$error_text="Invalid age";
}


$day=substr($cnp,-8,2);
$month=substr($cnp,-10,2);
$year=substr($cnp,-12,2);
$ddate=$year."-".$month."-".$day;

$time = strtotime($birth);

$newformat = date('y-m-d',$time);
echo $ddate."   ".$newformat;
if($newformat!=$ddate)
{
	$error=1;
	$error_text="CNP necorespunzator cu data nasterii";
}



$con = mysqli_connect(HOST,USER,PASSWORD,DATABASE);
	if (mysqli_connect_errno($con))
 {

		$error=1;
		$error_text="failed to connect";
  }

	if($email != "") {
		$sql1= "select * from register2 where email='$email'";
		if ($result = mysqli_query($con,$sql1))
		      $rowcount = mysqli_num_rows($result);
	if($rowcount>0)
		{
			$error=1;
			$error_text= "email existent";
		}}

		$sql2= "select * from register2" ;
		if ($result = mysqli_query($con,$sql2))
		      $rowcount = mysqli_num_rows($result);
			if( $rowcount>50)
			{
				$error=1;
				$error_text="nu mai exista locuri";
			}





if ($error==0){

$sql="INSERT INTO register2 (firstname,lastname,phone,email,cnp,facebook,birth,department,question,sex,facultate) VALUES('$firstname','$lastname','$phone','$email','$cnp','$facebook','$birth','$department','$question','$sex','$facultate')";

if ($con->query($sql) === TRUE) {
		echo "Succes";}
		else {
		    echo "Error: " . $sql . "<br>" . $con->error;
		}

}
else{ echo $error_text;
}
mysqli_close($con);
