-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2018 at 07:04 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ac`
--

-- --------------------------------------------------------

--
-- Table structure for table `register2`
--

CREATE TABLE `register2` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cnp` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `birth` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `question` varchar(255) NOT NULL,
  `sex` varchar(2) NOT NULL,
  `facultate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `register2`
--

INSERT INTO `register2` (`id`, `firstname`, `lastname`, `phone`, `email`, `cnp`, `facebook`, `birth`, `department`, `question`, `sex`, `facultate`) VALUES
(70, 'iarina', 'dsad', '0756039030', 'cristeaiarina03@gmail.com', '1234567891234', 'https://www.facebook.com/', '1998-02-02', 'edu', 'ddddddddddddddgggggggggggggg', 'M', 'cibernetica'),
(71, 'John', 'Doe', '', 'john@example.com', '', '', '0000-00-00', '', '', '', ''),
(72, 'iarina', 'dsad', '0756039030', 'cristeaiarina03@gmail.com', '1234567891234', 'https://www.facebook.com/', '1998-02-02', 'edu', 'ddddddddddddddgggggggggggggg', 'M', 'cibernetica'),
(73, 'iarina', 'dsad', '0756039030', 'cristeaiarina03@gmail.com', '1234567891234', 'https://www.facebook.com/', '1998-02-02', 'edu', 'ddddddddddddddgggggggggggggg', 'M', 'cibernetica'),
(74, 'iarina', 'dsad', '0756039030', 'cristeaiarina03@gmail.com', '1234567891234', 'https://www.facebook.com/', '1998-02-02', 'edu', 'ddddddddddddddgggggggggggggg', 'M', 'cibernetica'),
(75, 'iarina', 'dsad', '0756039030', 'cristeaiarina@gmail.com', '1234567891234', 'https://www.facebook.com/', '1998-02-02', 'edu', 'ddddddddddddddgggggggggggggg', 'M', 'cibernetica'),
(76, 'iarina', 'dsad', '0756039030', 'cristearina@gmail.com', '1234567891234', 'https://www.facebook.com/', '1998-02-02', 'edu', 'ddddddddddddddgggggggggggggg', 'M', 'cibernetica'),
(77, 'iarina', 'dsad', '0756039030', 'cristeari@gmail.com', '1234567891234', 'https://www.facebook.com/', '1998-02-02', 'edu', 'ddddddddddddddgggggggggggggg', 'M', 'cibernetica'),
(78, 'iarina', 'dsad', '0756039030', 'cristei@gmail.com', '1234567891234', 'https://www.facebook.com/', '1998-02-02', 'edu', 'ddddddddddddddgggggggggggggg', 'M', 'cibernetica'),
(79, 'iarina', 'dsad', '0756039030', 'crisi@gmail.com', '1234567891234', 'https://www.facebook.com/', '1998-02-02', 'edu', 'ddddddddddddddgggggggggggggg', 'M', 'cibernetica'),
(80, 'iarina', 'dsad', '0756039030', 'crii@gmail.com', '1234567891234', 'https://www.facebook.com/', '1998-02-02', 'edu', 'ddddddddddddddgggggggggggggg', 'M', 'cibernetica'),
(81, 'iarina', 'dsad', '0756039030', 'iffffff4gfffffddfgfi@gmail.com', '2980803336385', 'https://www.facebook.com/', '1998-08-03', 'edu', 'ddddddddddddddgggggggggggggg', 'F', 'cibernetica');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `register2`
--
ALTER TABLE `register2`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `register2`
--
ALTER TABLE `register2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
